"""
Demo class for the streamlit GUI.
"""
import os
from pathlib import Path

import httpx
import pandas as pd
import streamlit as st

from your_project_name.main import calculate_orbit_velocity
from your_project_name.helpers import get_recent_project, load_file, set_sys_arch
from your_project_name.helpers import get_list_of_all_project_infos, get_project_info, set_project_name
from your_project_name.helpers import get_mission_info, set_trained_model
from your_project_name.helpers import set_mission_orbit


st.set_page_config(layout="wide")

def app():
    if 'project_name' not in st.session_state:
        st.session_state['project_name'] = "None Selected"
        st.session_state['project_id'] = None

    proj_list = get_list_of_all_project_infos()
    #st.write(proj_list)
    project_idname_list = [(project["id"], project["project_name"]) for project in proj_list]


    # st.session_state["project_name"] = None
    col01,col02, col03 , col04= st.columns([4,1,1,1])
    with col01:
        st.title("Repo Template GUI")

    with col02:
        if st.button("Get recent project"):
            # (last_used_project_id, last_used_project_name) = get_recent_project()
            # st.session_state["project_id"] = last_used_project_id
            # st.session_state["project_name"] = last_used_project_name
            (st.session_state["project_id"], st.session_state["project_name"]) = get_recent_project()
    with col03:
        # Let the user select a project from the list of projects in the project DB
        selected_project = st.selectbox("Select a project",
                                    project_idname_list,
                                    index=None,
                                    placeholder=st.session_state["project_name"])
                                    # placeholder="Select an active project ...")
        if selected_project is not None:
            st.session_state["project_id"] = selected_project[0]
            st.session_state["project_name"] = selected_project[1]
    with col04:
        st.metric(label="Active project", value=st.session_state['project_name'])


    st.write("Welcome to the Default GUI!")

    st.write("This is a demo function from the main script. Enter an orbit altitude in km:")
    altitude = st.number_input("Altitude", value=450, step=1, format="%d")
    m_earth = 5.9721e24  # kg
    r_earth = 6371  # km
    st.write(
        "Your orbital velocity in m/s: ",
        calculate_orbit_velocity(planet_mass=m_earth, radius=(r_earth + altitude) * 1000),
    )

        # File paths using pathlib
    base_path = Path('your_project_name/')
    mission_file = base_path / 'data/mission_opssat.json'
    sys_arch_file = base_path / 'data/sys_arch_opssat.json'

    # Load and display data
    col11, col12 = st.columns(2)
    with col11:
        st.write("Mission Data")
        if st.button('Load Mission Data'):
            mission_data = load_file(mission_file)
            st.json(mission_data)

    with col12:
        st.write("System Architecture Data")
        if st.button('Load System Architecture Data'):
            sys_arch_data = load_file(sys_arch_file)
            st.json(sys_arch_data)
            df = pd.json_normalize(sys_arch_data)
            st.dataframe(df)

    st.divider()
    st.divider()
    st.title("The following are examples on how to call things from the backend.")
    st.divider()
    st.divider()


    if st.button("Get all project infos"):
        st.write(get_list_of_all_project_infos())


    with st.expander("all project ids"):
        if st.button("Get list of all projects IDs .."):
            st.write([project["id"] for project in get_list_of_all_project_infos()])

            # The above is the same as the below:
            # response = get_list_of_all_project_infos()
            # for project in response:
            #     st.write(f"ID: {project['id']}")
            # st.write(response)


    if st.button(f"Get Project info for {st.session_state['project_name']}, based on {st.session_state['project_id']}"):
        st.write(get_project_info(project_id=st.session_state["project_id"]))

    with st.expander("Project Name"):
        col31,col32 = st.columns(2)
        with col31:
            p_name = st.text_input(label="Define or update a project name:", placeholder="Project name")
        with col32:
            st.write(f"You selected as a project name: {p_name}")
            if st.button("Set Project name"):
                # status, msg = set_project_name(project_id=st.session_state["project_id"], project_name=p_name)
                msg = set_project_name(project_id=st.session_state["project_id"], project_name=p_name)
                # if status == 307:
                #     st.warning(msg)


    # =====================================================
    # =========== Mission Info ============================
    # =====================================================
    st.header("Dealing with Mission Information")

    # Select Celestical Body
    celestical_object = st.selectbox(label="Select the orbited celestical body",
                                     index=None,
                                     placeholder="Celestical Object",
                                     options=["Earth", "Luna", "Sun", "Mars"]
                                     )

    # Select Orbit Shell for Earth
    orbital_shell = st.selectbox(label="Select the orbital shell",
                                 placeholder="Orbital Shell",
                                 index=None,
                                 options=["LEO", "MEO", "GEO", "Cis-Lunar"]
                                 )

    # Keplerian elements for ISS orbit with starting values
    keplerian_elements = {
        "Semi-major Axis (km)": 6778.0,
        "Eccentricity": 0.001,
        "Inclination (degrees)": 51.6,
        "RAAN (degrees)": 15.6967,
        "Argument of Periapsis (degrees)": 0.6633,
        "True Anomaly (degrees)": 0.0  # Placeholder value
    }

    # Create columns for each element
    cols = st.columns(6)

    # Generate a number input for each element with its starting value
    for i, (element, value) in enumerate(keplerian_elements.items()):
        with cols[i]:
            value = round(st.number_input(element, value=value, key=element, format="%.5f"),5)
            # st.write(value, element)
            keplerian_elements[element]=value

    orbit_info = {
                "celestial_object": celestical_object,
                "orbit_shell": orbital_shell,
                "altitude_km": 0,
                "epoch_utc": "string",
                "semi_major_axis_km": keplerian_elements["Semi-major Axis (km)"],
                "eccentricity": keplerian_elements["Semi-major Axis (km)"],
                "inclination_degrees": keplerian_elements["Inclination (degrees)"],
                "perigee_height_km": 0,
                "apogee_height_km": 0,
                "raan_degrees": keplerian_elements["RAAN (degrees)"],
                "argument_of_perigee_degrees": keplerian_elements["Argument of Periapsis (degrees)"],
                "mean_anomaly_at_epoch_degrees": 0,
                "revolutions_per_day": 0,
                "orbit_number_at_epoch": 0,
                "time_eclipse_min": 0,
    }

    if st.button(f"Set Mission Info for {st.session_state['project_id']}"):
        if None in (celestical_object, orbital_shell):
            st.error("Please select Celestical Body and Orbit Shell to proceed.")
        else:
            st.write(orbit_info)
            response = set_mission_orbit(project_id=st.session_state["project_id"], orbit_info=orbit_info)
            st.write(response)
            # st.warning("Known errors with settin the orbit")

    if st.button(f"Get Mission Info for {st.session_state['project_id']}"):
        st.write(get_mission_info(project_id=st.session_state["project_id"]))







    # =====================================================
    # =========== System Info ============================
    # =====================================================

    st.header("System Wizard")
    sys_arch: dict = {
        "aodcs" : {},
        "com" : {},
        "eps" : {
            "battery" : {},
        },
        "obs" : {},
        "payload" : {},
    }
    sys_arch["aodcs"]["Enabled"] = st.checkbox("Attitude Orbit Determination Control System")
    sys_arch["com"]["Enabled"] = st.checkbox("Communication")
    sys_arch["eps"]["Enabled"] = st.checkbox("Electrical Power System")
    sys_arch["eps"]["battery"]["Enabled"] = st.checkbox("Battery")
    sys_arch["obs"]["Enabled"] = st.checkbox("Onboard Computer System")
    sys_arch["payload"]["Enabled"] = st.checkbox("Payload")
    if sys_arch["payload"]["Enabled"]:
        payload_camera = st.checkbox("Camera")
        if payload_camera:
            sys_arch["payload"]["camera"] = {}
            sys_arch["payload"]["camera"]["Enabled"] = payload_camera


    if st.button("Set System Architecture"):
        st.write(sys_arch)
        # response = set_sys_arch(project_id=st.session_state["project_id"], sys_arch=sys_arch)
        # st.write(response)
        st.warning("Not implemented yet. Ask JP for more info.")



    # =====================================================
    # =========== System Generator ============================
    # =====================================================

    st.divider()
    st. header("AI things")
    if st.button("Upload trained AI model"):
        st.write(f"Here is a list of trained AI models ready for upload for {st.session_state['project_name']}")
        set_trained_model(project_id=st.session_state["project_id"], model={})  # TODO Ask younes for model type
        st.warning("Not implemented yet. Ask JP for more info.")
        st.write("TODO: ")


    from your_project_name.helpers import _backend_post, BACKEND_URL, set_sys_generator

    sys_gen_info = {
        "agent": {
            "name_agent": "string",
            "type": "string",
            "placeholder": {},
        },
        "environment": {
            "environment": {},
                "system_generator_id": 0
            },
        "generated_design": {
            "comp_list_uids": [],
            "comp_list_types": [],
            "system_generator_id": 0
        }
    }

    response = set_sys_generator(project_id=st.session_state['project_id'],
                                 sys_gen_info=sys_gen_info)




    project_info = {
        "project_name": "string",
        "description": "string",
        "open_source": False,
        "team_members": [],
        "modified": "2024-04-15 09:20:53.259271",
        "website": "string",
        "picture_web": "string",
        "COMET_model_info": {},
        }
    if st.button("localhost"):
        sts, msg = _backend_post(endpoint="http://localhost:8000/api/v2/projects/", data=project_info)
    if st.button("backend"):
        response = set_sys_generator(project_id=st.session_state['project_id'],
                                 sys_gen_info=sys_gen_info)
        sts, msg = _backend_post(endpoint="http://backend:8000/api/v2/projects/", data=project_info)
        st.write(msg)
    if st.button("backend2"):
        sts, msg = _backend_post(endpoint=f"{BACKEND_URL}/v2/projects/", data=project_info)
        st.write(msg)



# This check allows the script to be run standalone (useful for development/testing)
if __name__ == "__main__":
    app()
