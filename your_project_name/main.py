"""
A simple function to show functionality.
"""


def demo_function() -> int:
    """This is a test function to test general functionality.
    It can easily be used to test an function import and it shows the
    reader general documentation style.
    """
    return 42


def calculate_orbit_velocity(planet_mass: float, radius: float) -> float:
    """Calculate the orbital velocity of an object around a planet.

    Args:
        planet_mass (float): mass of the planet in kg
        radius (float): radius of the planet in meters

    Returns:
        float: orbital velocity in m/s
    """
    g = 6.67430e-11  # gravitational constant in m^3/kg*s
    return (g * planet_mass / radius) ** 0.5


def main():
    """Calculate the orbital velocity of an object around a planet."""

    m_earth = 5.9721e24  # kg
    r_earth = 6371  # km
    try:
        altitude = int(input("Input of your space asset altitude in km: "))
    except EOFError:
        print("Defaulting to 6821km altitude as input, as no input is available.")
        altitude = int(450)
    print(
        "Your orbital velocity in m/s: ",
        calculate_orbit_velocity(planet_mass=m_earth, radius=(r_earth + altitude) * 1000),
    )


if __name__ == "__main__":
    main()
